<?php 
session_start();
include 'inc/connection.php';
include 'inc/function.php';
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UDUS | Cart</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/fontawesome-all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand"><b><i class="fa fa-utensils"></i> UDUS</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="login.php"><i class="fa fa-sign-in-alt"></i> Login<span class="sr-only">(current)</span></a></li>
            <li><a href="table.php"><i class="fa fa-table"></i> Table Booking</a></li>
          
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control"  id="navbar-search-input" style="width: 355%; border-radius: 3px;" placeholder="Search">
            </div>
          </form>
        </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li>
              <a href="cart.php">
                <i class="fa fa-shopping-cart"></i>
                <span class="hidden-xs">Cart <label class="label label-warning"><?php echo count(@$_SESSION['cart']) ?></label></span>
              </a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Table Booking
        </h1>
        <ol class="breadcrumb">
          <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Meal Booking</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="col-lg-6 thumbnail" style="height: 415px;">
                      <fieldset>
            <legend> <i class="fa fa-shopping-cart"></i> My Cart</legend>
              <div class="col-lg-6">
                <?php
                //var_dump($_SESSION['cart']);
            if(is_array($_SESSION['cart'])){
                /*
               echo $max=count($_SESSION['cart']);
                for($i=0;$i<$max;$i++){
                    $pid=$_SESSION['cart'][$i]['productid'];
                    $q=$_SESSION['cart'][$i]['qty'];

                    $pname=get_product_name($pid);
                    if($q==0) continue;
                    echo $pname.'<br>';

                    
                    }
                    */

                    foreach ($_SESSION['cart'] as $key => $value) {
                         $meal_id = $value['productid'];
                        echo $meal_name = get_meal_name($meal_id).' = '.get_price($meal_id).' NGN.  x '.$value['qty'].' Plates <a href="remove.php?meal_id='.$meal_id.'"><button class="btn btn-danger"  style="margin-left:242px;"><i class="fa fa-minus-circle"></i></button></a>';
                    }
?>
                    Delivery Option
                    <select id="d" class="form-control">
                      <option value="">Select Option</option>
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                   <?php echo "<hr>";?>
                   <input type="hidden" id="amount" value="<?php echo get_order_total() ?>">
                    <strong>TOTAL: <?php  echo get_order_total()?> NGN.</strong>
          <?php }
           
            ?>

               </div>
          </fieldset>
        </div>
        <div class="col-lg-6 thumbnail" style="height: 415px;">
          <div id="status" style="color: red"></div>
             <div class="col-lg-6">
                First Name
                <input type="text" id="firstname" autofocus class="form-control">
              </div>
              <div class="col-lg-6">
                Last Name
                <input type="text" id="lastname" class="form-control">
              </div>
              <div class="col-lg-6">
                Phone
                <input type="text" id="phone" class="form-control">
              </div>
               <div class="col-lg-6">
                Email
                <input type="email" id="email" class="form-control">
              </div>
               <div class="col-lg-12">
                Address
                <textarea id="address"  cols="30" rows="10" class="form-control"></textarea>
              </div>
              <div class="col-lg-6">
                <br>  
                 <script src='https://js.paystack.co/v1/inline.js'></script>
                    <button  id="pay" class='btn btn-success' onclick='payWithPaystack()' style='margin-left:34px;'><i class='fa fa-shopping-cart'></i> Check Out</button>
              </div>

<script>
  const selector = (element) =>  document.querySelector(element);
           
  function payWithPaystack(){
    let fname = selector('#firstname').value;
    let lname = selector('#lastname').value;
    let phone = selector('#phone').value;
    let email = selector('#email').value;
    let address = selector('#address').value;
    let option = selector('#d').value;
    
    if (option === 'Yes') {
    
      var amount = Number(selector('#amount').value) + 200;
    
    }else{
    
      var  amount = selector('#amount').value;
    
    }
    console.log(amount);
    var handler = PaystackPop.setup({
      key: 'pk_test_30bbe756476a4c8b47a3df7a2201cd711b16ceb1',
      email: email,
      amount: amount+"00",
      currency: "NGN",
      ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
      metadata: {
         custom_fields: [
            {
                display_name: "Mobile Number",
                variable_name: "mobile_number",
                value: "+23425152258"
            }
         ]
      },
      callback: function(response){
          alert('success. transaction ref is ' + response.reference);
            let ref = response.reference;

            const data = {
              "fname": fname,
              "lname": lname,
              "phone": phone,
              "email": email, 
              "address": address,
              "amount": amount, 
              "ref": ref
            }

            $.ajax({
                type: 'POST',
                url:'cart_checkout.php',
                data: data,
                success:function(x){
                  alert(x);
                }
               })
            
      },
      onClose: function(){
          alert('window closed');
      }
    });
    handler.openIframe();
  }
</script>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Designed By Asmau Tsamiya</b> 
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="">UDUS RESTAURANTS</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="app.js"></script>
<script>
  validator(selector("#firstname"),"First Name");
  validator(selector("#lastname"),"Last Name");
  validator(selector("#phone"),"Phone Number");
  validator(selector("#email"),"Email Address");
  validator(selector("#address"),"Address");
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->{

<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
