<?php
error_reporting(0);
 date_default_timezone_set("Africa/Lagos");
function clean($var) {
    // return mysql_real_escape_string(addslashes($var));
 }
 function getPage() {
  $page = preg_replace("/^\/[a-zA-Z.]+\//","",$_SERVER['PHP_SELF']);
  return $page;
 }
 function listRequest() {
   
 }
 function sendMail($to,$subject,$message) {
  $mail = new PHPMailer;
  $mail->isSMTP();
  $mail->SMTPDebug = 2;
  $mail->Debugoutput = 'html';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 587;
  $mail->SMTPSecure = 'tls';
  $mail->SMTPAuth = true;
  $mail->Username = "realtimechatsystem@gmail.com";
  $mail->Password = "umarbello01";
  $mail->setFrom('realtimechatsystem@gmail.com', 'REAL TIME CHAT');
  $mail->addReplyTo('realtimechatsystem@gmail.com', 'REAL TIME');
  $mail->addAddress($to);
  $mail->Subject = $subject;
  $mail->Body =$message;
  if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
     return true;
  }
 }
  function resizeImage($inputFileName,$destFolder,$perc,$quality=80) {
         $imageSizeType = getimagesize($_FILES[$inputFileName]['tmp_name']);
         $width = $imageSizeType[0];
         $height = $imageSizeType[1];
         $type = $imageSizeType['mime'];

         switch($type) {
          case "image/png":
             $image = imagecreatefrompng($_FILES[$inputFileName]['tmp_name']);
             break;
          case "image/jpeg": case "image/pjpeg": 
             $image = imagecreatefromjpeg($_FILES[$inputFileName]['tmp_name']);
             break;
            case "image/gif":
               $image = imagecreatefromgif($_FILES[$inputFileName]['tmp_name']);
               break;
         }
        
         $fileInfo = pathinfo($_FILES[$inputFileName]['name']);
         $extension = strtolower($fileInfo["extension"]);
         $newName = rand(0,99999999999).".".$extension;

         $new_width = $width * $perc;
         $new_height = $height * $perc;
         $new_canvas = imagecreatetruecolor($new_width, $new_height);

       //$image = imagecreatefromjpeg($_FILES[$image]['tmp_name']);
       imagecopyresampled($new_canvas, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

      switch($type) {
          case 'image/png': 
          imagepng($new_canvas, $destFolder.$newName);
          break;
            case 'image/gif': 
          imagegif($new_canvas, $destFolder.$newName); 
          break;          
            case 'image/jpeg': case 'image/pjpeg': 
          imagejpeg($new_canvas, $destFolder.$newName, $quality); 
            //default: return false;
        }

      return $newName;
  }
  function error($message) {
	 echo "<div class='col-lg-12 alert alert-danger' style='padding:10px'><i class='fa fa-warning'></i> $message <span class='close' data-dismiss='alert'>x</span></div>";
  }
  function errorArray($message) {
   echo "<div class='col-lg-12 alert alert-danger' style='padding:10px'><i class='fa fa-warning'></i>";
   echo "<b> The following error(s) occured.</b><ol>";
   foreach($message as $m)
     echo "- ".$m."<br/>";
   echo "</ol>";
   echo "</div>";

 }
  function success($message) {
     echo "<div class='alert alert-success' style='padding:10px'><i class='fa fa-information'></i> $message <span class='close'>x</span></div>";
  }
  function getGroupId($group_name) {
    global $pdo;
    $query = $pdo->prepare("SELECT * FROM groups WHERE name='$group_name'");
    $query->execute();
    $row = $query->fetch();
    return $row['group_id'];
  }
  function listGroupMembers($group_id) {
     global $pdo;
     $memeber = array();
     $query = $pdo->prepare("SELECT * FROM groups 
                             INNER JOIN group_users USING(group_id)
                             INNER JOIN user ON user.user_id=group_users.friend_id
                             INNER JOIN user_bio ON user_bio.user_id = group_users.friend_id
                             WHERE groups.group_id = ?
                            ");
     $query->execute(array($group_id));
     while($row = $query->fetch()) {
        $memeber[] = array(
                         'username' => $row['username'],
                         'user_id' => $row['friend_id'],
                         'fullname' => $row['first_name']. ' '.$row['other_names'],
                         'date_added' => $row['date_added']
                     );
     }
     return $memeber;
  }
  function listGroups($user_id) {
       global $pdo;
       $group = array();

       $query = $pdo->prepare("SELECT * FROM groups WHERE user_id=?");
       //echo 1;
       $query->execute(array($user_id));
      
       while($row = $query->fetch()) {
          $group[] = array('name'=>$row['name'],'group_id'=>$row['group_id'],'admin_id'=>$row['user_id'],'date'=>$row['creation_date']);
       }

       $query2 = $pdo->prepare("SELECT g.group_id,g.name,g.user_id,creation_date FROM groups g INNER JOIN group_users USING(group_id)
                              WHERE friend_id=?");
       $query2->execute(array($user_id));
       while($row2 = $query2->fetch()) {
          $group[] = array('name'=>$row2['name'],'group_id'=>$row2['group_id'],'admin_id'=>$row2['user_id'],'date'=>$row2['creation_date']);
       }
       return $group;
  }

  function unreadMessages($user_id,$friend_id){
      global $pdo;
      $query = $pdo->prepare("SELECT id FROM chat WHERE user_id=? AND friend_id=?
                             AND is_read = 0");
      $query->execute(array($friend_id,$user_id));
      $query2 = $pdo->prepare("SELECT id FROM chat WHERE friend_id=? AND user_id=?
                             AND is_read = 0");
      //echo $query2->queryString;
      $query2->execute(array($friend_id,$user_id));
      //if($query->rowCount() > 0)
         return $query->rowCount();

      //elseif($query2->rowCount() > 0 )
        // return $query2->rowCount();
      //else
        // return 0;
  }

   function listFriends($user_id) {
      global $pdo;
  	  $friends = array();

  	  $query = $pdo->prepare("SELECT username,status,first_name,other_names,u.user_id,last_seen FROM user_friend uf INNER JOIN user u ON uf.friend_id=u.user_id
                            INNER JOIN user_history uh ON uh.user_id=u.user_id
                            INNER JOIN user_bio ub ON ub.user_id = u.user_id
  	  	                   WHERE uf.user_id=?");

      $query->execute(array($user_id));
      
  	  while($row = $query->fetch()) {
  	  	  $isOnline =  ((strtotime($row['last_seen'])+3) >= time() ? 1 : 0);
          $username = @$row['username'];

          $friends[] = array(
                             'isOnline'=>$isOnline,
                             'status'=> $row['status'],
                             'username'=>$username,
                             'user_id'=>$row['user_id'],
                             'unread' =>unreadMessages($user_id,$row['user_id']),
                             'fullname'=>$row['first_name'].' '.$row['other_names']
                       );
  	  }


      function isMyFriend($username) {
          $friends =  listFriends($_SESSION['user_id']);
          $f = array();
          foreach($friends as $friend) {
                           $f[] = $friend['username'];
          }
          return (in_array($username,$f));
      }
     

  	  $query2 = $pdo->prepare("SELECT username,status,first_name,other_names,u.user_id,last_seen FROM user_friend uf INNER JOIN user u ON uf.user_id=u.user_id
                             INNER JOIN user_history uh ON uh.user_id=u.user_id
                              INNER JOIN user_bio ub ON ub.user_id = u.user_id
  	  	                   WHERE uf.friend_id=? and status=1");
      $query2->execute(array($user_id));
      //echo $query2->rowCount();
  	  while($row2 = $query2->fetch()) {
  	  	  $isOnline = ((strtotime($row2['last_seen'])+3) >= time() ? 1 : 0);
          $username = @$row2['username'];
          $friends[] = array(
                              'isOnline'=>$isOnline,
                              'username'=>$username,
                              'status' => $row2['status'],
                              'user_id'=>$row2['user_id'],
                              'unread' =>unreadMessages($user_id,$row2['user_id']),
                              'fullname'=>$row2['first_name'].' '.$row2['other_names']
                        );
  	  }
      //var_dump($friends);
  	  return $friends;

  }
  
  function chatTime($strTime) {
      $ts = strtotime($strTime);
      $now = time();
      if($now - $ts < (6*60*60))
          return  date("g:i a ",$ts);
      elseif($now - $ts < (12*60*60))
          return "Today at ". date("g:i a ",$ts);
      elseif($now - $ts < (48 * 60 * 60)) 
          return "Yesterday at ". date("g:i a ",$ts);
      else
          return date('l, F d, Y \a\t g:i a');
  }
  function getUserId($username) {
      global $pdo;
      $query = $pdo->prepare("SELECT user_id FROM user WHERE username=?");
      $query->execute(array($username));
      $row = $query->fetch();

      return $row['user_id'];
  }
   function remove_meal($pid){
    $pid=intval($pid);
    $max=count($_SESSION['cart']);
    for($i=0;$i<$max;$i++){
      if($pid==$_SESSION['cart'][$i]['productid']){
        unset($_SESSION['cart'][$i]);
        break;
      }
    }
    $_SESSION['cart']=array_values($_SESSION['cart']);
  }
  function get_order_total(){
    $max=count($_SESSION['cart']);
    $sum=0;
    for($i=0;$i<$max;$i++){
      $pid=$_SESSION['cart'][$i]['productid'];
      $q=$_SESSION['cart'][$i]['qty'];
      $price=get_price($pid);
      $sum+=$price*$q;
      
    }
    return $sum;
  }
  function addtocart($pid,$q){
    if($pid < 1 or $q < 1) return;
    
    if(is_array($_SESSION['cart'])){
      if(product_exists($pid)) return;
      $max=count($_SESSION['cart']);
      $_SESSION['cart'][$max]['productid']=$pid;
      $_SESSION['cart'][$max]['qty']=$q;
    }
    else{
      $_SESSION['cart']=array();
      $_SESSION['cart'][0]['productid']=$pid;
      $_SESSION['cart'][0]['qty']=$q;
    }
  }
  function product_exists($pid){
    $pid=intval($pid);
    $max=count($_SESSION['cart']);
    $flag=0;
    for($i=0;$i<$max;$i++){
      if($pid==$_SESSION['cart'][$i]['productid']){
        $flag=1;
        break;
      }
    }
    return $flag;
  }
   function get_meal_name($pid){
    global $db;
    $result= $db->query("SELECT product_name FROM products WHERE product_id= $pid");
    $row= $result->fetch_array(MYSQLI_ASSOC);
    return $row['product_name'];
  }
   function get_price($pid){
    global  $db;
    $result= $db->query("SELECT product_price FROM products where product_id=$pid");
    $row=$result->fetch_array(MYSQLI_ASSOC);
    return $row['product_price'];
  }
?>