<?php 
include 'inc/connection.php';
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UDUS | Booking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/fontawesome-all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand"><b><i class="fa fa-utensils"></i> UDUS</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="login.php"><i class="fa fa-sign-in-alt"></i> Login<span class="sr-only">(current)</span></a></li>
            <li><a href="table.php"><i class="fa fa-table"></i> Table Booking</a></li>
          
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control"  id="navbar-search-input" style="width: 355%; border-radius: 3px;" placeholder="Search">
            </div>
          </form>
        </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li>
              <a href="cart.php">
                <i class="fa fa-shopping-cart"></i>
                <span class="hidden-xs">Cart <label class="label label-warning"><?php echo count(@$_SESSION['cart']) ?></label></span>
              </a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Table Booking
        </h1>
        <ol class="breadcrumb">
          <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Table Booiking</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="col-lg-6 thumbnail" style="height: 369px;">
            <img src="table.png" alt="">
        </div>
        <div class="col-lg-6 thumbnail" style="height: 369px;">
          <fieldset>
            <legend> <i class="fa fa-table"></i> Book for Table</legend>
            <form action="table_process.php" method="POST">
              <div class="col-lg-6">
                Restaurant
                <select name="rest_id" id="" class="form-control">
                  <?php 
                    $rest = mysqli_query($db,"SELECT * FROM resturants");

                    while ($getr = mysqli_fetch_array($rest)) {
                          echo "<option value='".$getr['rest_id']."'>".$getr['rest_name']."</option>";
                    }

                   ?>
                </select>
              </div>
              <div class="col-lg-6">
                People in Number
                <input type="text" name="table" class="form-control">
              </div>
              <div class="col-lg-6">
                Time
                <input type="time" name="time" class="form-control">
              </div>
              <div class="col-lg-6">
                Date
                <input type="date" name="date" class="form-control">
              </div>
              <div class="col-lg-6">
               <br>
                <input type="submit" name="book" value="Place Booking" class="btn btn-success">
              </div>
            </form>
          </fieldset>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Designed By Asmau Tsamiya</b> 
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="">UDUS RESTAURANTS</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
