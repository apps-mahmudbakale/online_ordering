<?php 
include '../inc/connection.php';
include '../inc/function.php';

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Snacks | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css/fontawesome-all.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">

  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red-light sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><i class="fas fa-utensils"></i></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><i class="fas fa-utensils"></i> Snacks</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" readonly placeholder="">
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
          <li>
          <a href="products.php">
            <i class="fa fa-gift"></i> <span>Products</span>
          </a>
        </li>
        <li>
          <a href="users.php">
            <i class="fa fa-users"></i> <span> Users</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="col-lg-12">
      <div class="panel panel-danger" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-folder-open'></i> New Product</div>
        <div class="statusMsg">
          <?php 
          if (isset($_POST['submit'])) {
            $product_name = $_POST['product_name'];
            $product_price = $_POST['product_price'];
            $product_stock = $_POST['product_stock'];
            if(!empty($_FILES['product_caption']['name']))
                     $name =  resizeImage('product_caption','../captions/',0.3);
                  else
                    $name = '';

                  $query = mysqli_query($db,"INSERT INTO `products`(`product_id`, `product_name`, `product_price`, `product_stock`, `product_caption`) VALUES (NULL,'$product_name','$product_price','$product_stock','$name')");

                  if ($query) {

                  success('Product Added Successfuly');?>

             <script>
              setTimeout(function(){
                window.location='products.php';
              }, 7000)
            </script>

                    
                  <?php 
                }

                      }


           ?>
        </div>
          <div class="panel-body">
            <form  id="fupForm" action="" method="post" enctype="multipart/form-data">
              <div class="col-lg-6">
                    Product Name
                   <input type="text" id="product_name" name="product_name" class="form-control">
                 </div> 
                 <div class="col-lg-6">
                  Product Price
                   <input type="text" id="product_price" name="product_price"  class="form-control">
                 </div>
                 <div class="col-lg-6">
                  Product In Stock
                   <input type="text" id="product_stock" name="product_stock"  class="form-control">
                 </div>
                  <div class="col-lg-6">
                  Product Caption
                   <input type="file" id="product_caption" name="product_caption"  class="form-control">
                 </div>
        </div>
        <div class='panel-footer'>
            <button  type="submit"  name="submit" class='btn btn-success submitBtn'><i class="fa fa-save"></i> Save</button>
           <!--  <a href="" class="btn btn-success pull-right">Batch Upload Doctors</a> -->
        </div>
         </form>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="panel panel-danger" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-table'></i> Products</div>
        <div id="status"></div>
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <th>S/N</th>
                <th>Product Name</th>
                <th>Product Price</th>
                <th>Product Stock</th>
                <th>Action</th>
              </thead>
              <tbody>
                <?php 
                  $query = mysqli_query($db,"SELECT * FROM products");
                  $sn=0;
                  while ($row = mysqli_fetch_array($query)) {
                    $sn++;
                    echo "<tr>
                          <td>".$sn."</td>
                          <td>".$row['product_name']."</td>
                          <td>&#8358;".number_format($row['product_price'],2)."</td>
                          <td>".$row['product_stock']."</td>
                          <td><a href='' class='btn btn-default'><i class='fa fa-eye'></i></a><a href='' class='btn btn-default'><i class='fa fa-edit'></i></a><a href='' class='btn btn-default'><i class='fa fa-trash'></i></a></td>
                          </tr>";
                  }

                 ?>
              </tbody>
            </table>
        </div>
        <div class='panel-footer'>
           
        </div>
         
      </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
        <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="">Snacks</a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/jquery-1.10.0.js"></script>
<script src="js/jquery-1.8.3.js"></script>
<script src="js/jquery-3.1.4.min.js"></script>
</body>
</html>
