  const validator = (element, fieldName) => {
      element.addEventListener('focusout', () => {
          let value = element.value;

          if (value === "") {
              selector("#status").textContent =  `${fieldName} is required`;
              selector("#pay").disabled = true;
          } else {
            selector("#status").textContent = "";
            selector("#pay").disabled = false;
          }
      })  
  }
