<?php
session_start();
/*
if (!isset($_SESSION['name'])) {
    echo '<script>alert("Please Login To Access This Page"); window.location = "index.php";</script>';
}

 //require_once 'conx.php';
 //require_once 'session.php';
$session = $_SESSION['name'];

 $fetch = $connection->query("SELECT * FROM user WHERE adm_no = $session");

 while ($data = $fetch->fetch_assoc()) {
     $fname = $data['first_name'];
     $lname = $data['last_name'];
     $level = $data['level'];
      $user_id = $data['id'];
     $course_id = $data['course_id'];

 }


$query = $connection->query("SELECT * FROM courses WHERE course_id = $course_id");
$row = $query->fetch_assoc();
$course = $row['course_name'];
*/
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UDUS e-Library | Home</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-green-light sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="home.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>UDUS</b>Lib</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>UDUS</b>e-Library</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->



                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="images/avatar.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $fname;?>&nbsp;<?php echo $lname;?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="images/avatar.png" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $fname;?>&nbsp;<?php echo $lname;?> - Student
                                    <small> Level: <?php echo $level;?></small>
                                </p>
                            </li>
                           
                            
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="logout.php" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="images/avatar.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $fname;?>&nbsp;<?php echo $lname;?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="hidden" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                <?php
                $query = $connection->query("SELECT * FROM libraries WHERE user = '$user_id'");
                $count = $query->num_rows;

                ?>
                    <a href="#">
                    
                        <i class="fa fa-book"></i> <span>My Library</span> <i class="fa fa-angle-left pull-right"></i>
                        <span class="label label-primary pull-right"><?php echo $count;?></span>
                    </a>
                    <ul class="treeview-menu">
                         <?php 
                        while ($lib = $query->fetch_assoc()) {
                        ?>
                        <li><a href="book.php?book=<?php echo $lib['book_id'];?>"><i class="fa fa-file-pdf-o"></i><?php echo $lib['book_name'];?></a></li>
                        <?php
                        }

                        ?>
                    </ul>
                </li>
                <li class="treeview">
                <?php
                $que = $connection->query("SELECT * FROM `recent` r INNER JOIN books b ON r.book_id = b.id  WHERE user_id='$user_id'");    
                 $count1 = $que->num_rows;
                ?>
                    <a href="#">
                        <i class="fa fa-clock-o"></i>
                        <span>Recent Readings</span>
                        <span class="label label-primary pull-right"><?php echo $count1;?></span>
                    </a>
                    
                    <ul class="treeview-menu">
                    <?php
                        while ($a = $que->fetch_array(MYSQLI_ASSOC)) {?>
                             <li><a href="book.php?id="><i class="fa fa-file-pdf-o"></i><?php echo $a['name'];?> </a></li>
                       <?php }

                    ?>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span>Other Categories</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                        <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                        <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                        <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                    </ul>
                </li>


        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
              <i class="fa fa-book"></i>  Books From Your Course
                <form role="form" action="" method="post">
                    <div class="input-group"><input type="text" class="form-control" name="search" placeholder="Search For Books">
                     <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                    </div>
                    </form>
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Home</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <?php
               

                $query = "SELECT * FROM books WHERE area = '$course'";
                $result = $connection->query($query);
                while($row = $result->fetch_assoc()) {
                    ?>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua-gradient ">
                            <div class="inner" style="text-align: center">
                                <i class="fa fa-file-pdf-o fa-5x text-red"></i>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="book.php?book=<?php echo $row['id'];?>" class="small-box-footer"><?php echo $row['name'];?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div><!-- ./col -->
                    <?php
                }
                ?>

            </div><!-- /.row -->
            <!-- Main row -->

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2014-<?php echo date('Y');?> <a href="#">UDUS e-Library</a>.</strong> All rights reserved.
    </footer>


</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="js/jQuery-2.1.4.min.js"></script>

<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="js/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="js/demo.js"></script>
</body>
</html>
