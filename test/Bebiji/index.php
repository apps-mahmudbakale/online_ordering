﻿<?php
session_start();
include_once 'connect.php';

if (isset($_POST['submit'])) {
	$username = $_POST['username'];
	$password = md5($_POST['password']);

	$query = $connection->query("SELECT * FROM login   WHERE username = '$username' AND password = '$password'");
	$row = $query->fetch_array(MYSQLI_ASSOC);
	if ($row > 0) {
		if ($row['role_id'] == 1) {
			$_SESSION['id'] = $row['login_id'];
			$connection->query("INSERT INTO `login_audit`(`audit_id`, `login_id`, `last_login`) VALUES (NULL,'$row[login_id]',NOW())");
			header('Location:Admin/');
		} elseif ($row['role_id'] == 2) {
			$_SESSION['id'] = $row['login_id'];
			$connection->query("INSERT INTO `login_audit`(`audit_id`, `login_id`, `last_login`) VALUES (NULL,'$row[login_id]',NOW())");
			header('Location:home.php');
		}
	}
	
}



?>
<!DOCTYPE html>
<html lang="en">
	<head>
    	<!-- 
    	Boxer Template
    	http://www.templatemo.com/tm-446-boxer
    	-->
		<meta charset="utf-8">
		<title>Estate Management System</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

		<!-- animate css -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- google font -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,800' rel='stylesheet' type='text/css'>

		<!-- custom css -->
		<link rel="stylesheet" href="css/templatemo-style.css">

	</head>
	<body>
		<!-- start preloader -->
		<div class="preloader">
			<div class="sk-spinner sk-spinner-rotating-plane"></div>
    	 </div>
		<!-- end preloader -->
		<!-- start navigation -->
		<nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">ESTATES MANAGEMENT </a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right text-uppercase">
						<li><a href="#home"><i class="fa fa-home"></i> Home</a></li>
						<li><a href="#post" data-toggle="modal"><i class="fa fa-sign-in"></i> Sign In</a></li>
						<li><a href=""><i class="fa fa-edit"></i>Sign Up </a></li>
						<li><a href="#feature"><i class="fa fa-question-circle"></i> About Us</a></li>
						<li><a href="#pricing"><i class="fa fa-money"></i> Pricing</a></li>
						<li><a href="#contact"><i class="fa fa-phone"></i> Contact</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- end navigation -->
		<!-- start home -->
		<section id="home">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10 wow fadeIn" data-wow-delay="0.3s">
							<br>
							<br>
							<img src="images/home-bg.jpg" class="img-responsive img-rounded img-thumbnail" alt="home img">
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
			</div>
		</section>
		<!-- end home -->
		<!-- start divider -->
		<section id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-4 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
						<i class="fa fa-laptop"></i>
						<h3 class="text-uppercase">LUXIRUABILITY</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
					</div>
					<div class="col-md-4 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
						<i class="fa fa-twitter"></i>
						<h3 class="text-uppercase">COMFORTABILiTY</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
					</div>
					<div class="col-md-4 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
						<i class="fa fa-font"></i>
						<h3 class="text-uppercase">AFFORDABILITY</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
					</div>
				</div>
			</div>
		</section>
		<!-- end divider -->

		<!-- start feature -->
		<section id="feature">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s">
						<h2 class="text-uppercase">Our ESTATES</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p><span><i class="fa fa-mobile"></i></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p><i class="fa fa-code"></i>Quis autem velis reprehenderit et quis voluptate velit esse quam.</p>
					</div>
					<div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
						<img src="images/IMG_3670.jpg" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end feature -->

		<!-- start feature1 -->
		<section id="feature1">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
						<img src="images/IMG_3670.jpg" class="img-responsive" alt="feature img">
					</div>
					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
						<h2 class="text-uppercase">More About Alkanchi Estates</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p><span><i class="fa fa-mobile"></i></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p><i class="fa fa-code"></i>Quis autem velis reprehenderit et quis voluptate velit esse quam.</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end feature1 -->

		<!-- start pricing -->
		<section id="pricing">
			<div class="container">
				<div class="row">
					<div class="col-md-12 wow bounceIn">
						<h2 class="text-uppercase">Our Pricing</h2>
					</div>
					<div class="col-md-4 wow fadeIn" data-wow-delay="0.6s">
						<div class="pricing text-uppercase">
							<div class="pricing-title">
								<h4>Basic Plan</h4>
								<p></p>
								<small class="text-lowercase"></small>
							</div>
							<ul>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
							<button class="btn btn-primary text-uppercase">Sign up</button>
						</div>
					</div>
					<div class="col-md-4 wow fadeIn" data-wow-delay="0.6s">
						<div class="pricing active text-uppercase">
							<div class="pricing-title">
								<h4>Normal Plan</h4>
								<p>NGN.150,000</p>
								<small class="text-lowercase">Yearly</small>
							</div>
							<ul>
								<li>1 Bedroom Flat</li>
								<li>1 Parlour</li>
								<li>1 Toilet</li>
								<li>1 Extra Toilet</li>
							</ul>
							<button class="btn btn-primary text-uppercase">Sign up</button>
						</div>
					</div>
					<div class="col-md-4 wow fadeIn" data-wow-delay="0.6s">
						<div class="pricing text-uppercase">
							<div class="pricing-title">
								<h4>Pro Plan</h4>
								<p>NGN. 250,000</p>
								<small class="text-lowercase">Yearly</small>
							</div>
							<ul>
								<li>2 Bedroom Flat</li>
								<li>1 Parlour</li>
								<li>2 Toilets For Each Room</li>
								<li>1 Extra Toilet</li>
							</ul>
							<button class="btn btn-primary text-uppercase">Sign Up</button>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end pricing -->

		
		<!-- start contact -->
		<section id="contact">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
							<h2 class="text-uppercase">Contact Us</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
							<address>
								<p><i class="fa fa-map-marker"></i>Street Name, City Name, Nigeria</p>
								<p><i class="fa fa-phone"></i> Phones</p>
								<p><i class="fa fa-envelope-o"></i> info@company.com</p>
							</address>
						</div>
						<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
							<div class="contact-form">
								<form action="#" method="post">
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Name">
									</div>
									<div class="col-md-6">
										<input type="email" class="form-control" placeholder="Email">
									</div>
									<div class="col-md-12">
										<input type="text" class="form-control" placeholder="Subject">
									</div>
									<div class="col-md-12">
										<textarea class="form-control" placeholder="Message" rows="4"></textarea>
									</div>
									<div class="col-md-8">
										<input type="submit" class="form-control text-uppercase" value="Send">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end contact -->

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row">
					<p>Copyright © <?php echo date('Y');?> Your Company Name</p>
				</div>
			</div>
		</footer>
		<!-- end footer -->
        
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/jquery.singlePageNav.min.js"></script>
		<script src="js/custom.js"></script>

<!--Modal post-->
      <div class="modal modal-primary fade" id="post" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                      <h4 class="modal-title">User Login</h4>
                  </div>
                  <div class="modal-body">
                  <div class="panel panel-success">
                  	<div class="panel-heading">Enter Details to Login</div>
                  	<div class="panel-body">
                     <form action=""  method="post" role="form">
                         <div class="form-group">
                             <input type="text" id="username" name="username" placeholder="Username" required class="form-control">
                             <div id="title_text" style="color:#a9302a"></div>
                         </div>
                         <div class="form-group">
                             <input type="password" id="password" placeholder="Password" class="form-control" required name="password">
                             <div id="text_text" style="color: #a9302a"></div>
                         </div>
                         

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                      <button type="submit"  id="submit" name="submit" class="btn btn-success"><i class="fa fa-sign-in"></i>Login</button>
                  </div>
                  </form>
                  </div>
                  </div>
				  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
		  <div class="modal modal-primary fade" id="reg" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                  <?php if (isset($_POST['reg'])) {
                  	$username = $_POST['username'];
                  	$password = md5($_POST['password']);
                  	$fname = $_POST['firstname'];
                  	$lname = $_POST['lastname'];
                  	$email = $_POST['email'];
                  	$phone = $_POST['phone'];
                  	$connection->query("INSERT INTO `login`(`login_id`, `username`, `password`, `first_name`, `last_name`, `phone`, `email`, `role_id`) VALUES (NULL,'$username','$password','$fname','$lname','$phone','$email','2')");
                  	echo "<script>alert('Registration Success'); window.location='index.php';</script>";
                  } ?>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                      <h4 class="modal-title">Sign Up</h4>
                  </div>
                  <div class="modal-body">
                  <div class="panel panel-success">
                  	<div class="panel-heading">Fill In These Fields</div>
                  	<div class="panel-body">
                     <form action=""  method="post" role="form">
                         <div class="form-group">
                             <input type="text" id="firstname"  name="firstname" placeholder="First Name" required class="form-control">
                             <div id="first_text" style="color:#a9302a"></div>
                         </div>
                         <div class="form-group">
                             <input type="text" id="lastname" name="lastname" placeholder="Last Name" class="form-control" required>
                             <div id="lastname_text" style="color: #a9302a"></div>
                         </div>
						  <div class="form-group">
                             <input type="email" id="email" name="email" placeholder="Email" class="form-control" required>
                             <div id="email_text" style="color: #a9302a"></div>
                         </div>
                          <div class="form-group">
                             <input type="text" id="phone" name="phone" placeholder="Mobile Phone" class="form-control" required>
                             <div id="phone_text" style="color: #a9302a"></div>
                         </div>
						 <div class="form-group">
                             <input type="text" id="username" name="username" placeholder="Username" class="form-control" required>
                             <div id="username_text" style="color: #a9302a"></div>
                         </div>
						 <div class="form-group">
                             <input type="password" id="password" name="password" placeholder=" Password" class="form-control" required>
                             <div id="password_text" style="color: #a9302a"></div>
                         </div>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                      <button type="submit"  id="submit" name="reg" class="btn btn-success"><i class="fa fa-sign-in"></i>Submit</button>
                  </div>
                  </form>
                  </div>
                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
	</body>
</html>