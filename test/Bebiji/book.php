<?php session_start(); ?>
<!DOCTYPE html>
<!-- saved from url=(0062) -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Customer| Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="css/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="css/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="css/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="css/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="css/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="css/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
  <body class="sidebar-mini skin-blue">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>CBN</b>Q</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CBN Quaters</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
             
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: auto;">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="images/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>customer</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            
              
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="home.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
            <li class="treeview">
              <a href="profile.php">
                <i class="fa fa-user"></i>
                <span>My Profile</span>
              </a>
            </li>
            <li class="treeview">
              <a href="book.php">
                <i class="fa fa-share"></i> <span>Make Bookings</span>
               
              </a>
            </li>
            <li class="treeview">
              <a href="">
                <i class="fa fa-comments"></i> <span>Forum</span>
                
              </a>
            </li>
            <li><a href="#"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Customer</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">       
       <div class="row">
            <!-- Left col -->
            <section class="col-lg-11  connectedSortable" style="margin-left:34px;">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="active"><a href="#revenue-chart" data-toggle="tab"></a></li>
                  <li><a href="#sales-chart" data-toggle="tab"></a></li>
                  <li class="pull-left header"><i class="fa fa-inbox"></i> </li>
                </ul>
                <div class="tab-content no-padding">
                <?php
                  include 'connect.php';
                  if (isset($_POST['book'])) {
                      $login_id = $_SESSION['id'];
                      $apart_id = $_POST['apart'];

                      $connection->query("INSERT INTO `bookings`(`booking_id`, `login_id`, `apart_id`, `date`) VALUES (NUll,'$login_id','$apart_id',NOW())");
                      echo "<script>alert('Apartment Booked'); window.location='book.php'</script>";
                  }
                ?>
                  <legend>Book Apartment</legend>
                  <form action="" method="POSt">
                    <div class="col-lg-6">
                      Apartments
                      <select name="apart" class="form-control">
                        <?php
                        $query = $connection->query("SELECT * FROM apartments");
                        while ($row = $query->fetch_array(MYSQLI_ASSOC)) {
                            echo"<option value='".$row['apart_id']."'>".$row['apart_name']."</option>";
                        }

                        ?>
                      </select>
                    </div>
                    <div class="col-lg-6">
                      <br>
                      <input type="submit" name="get" value="Get Details" class="btn btn-success"></input>
                    </div>
                  </form>

                  <?php
                  if (isset($_POST['get'])) {
                      $id = $_POST['apart'];
                      $query = $connection->query("SELECT * FROM apartments WHERE apart_id ='$id'");
                      $r = $query->fetch_array(MYSQLI_ASSOC);

                      echo "<table class='table table-striped'>
                      <tr>
                      <td>Apartment Name</td>
                      <td>Apartment Category</td>
                      <td>Apartment Description</td>
                      <td>Apartment price</td>
                      <td></td>
                      </tr>
                      <tr>
                      <td>".$r['apart_name']."</td>
                      <td>".$r['category']."</td>";



                        if ($r['category'] == 'Basic Plan') {
                          echo "<td>1 Bedroom, 1 Toilet</td>";
                          echo "<td>".$r['apart_price']."</td>";
                        }elseif ($r['category'] == 'Normal Plan') {
                          echo "<td>1 Bedroom, 1 Parlour, 1 Toilet, 1 Extra Toilet</td>";
                          echo "<td>".$r['apart_price']."</td>";
                        } elseif ($r['category'] == 'Pro Plan') {
                           echo "<td>2 Bedroom, 1 Parlour, 2 Toilet For Each, 1 Extra Toilet</td>";
                          echo "<td>".$r['apart_price']."</td>";
                        }
                        echo"<td><form action='' method='POST'><input type='hidden' name='apart' value='".$r['apart_id']."'><input type='submit' name='book' class='btn btn-primary' value='Book'></form></td>";
                      echo"</tr>
                      </table>" ;
                      }

                  ?>
                </div>
                </div>
                </div>
              </div><!-- /.nav-tabs-custom -->

              

             
             
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div>
              </div><!-- /.box -->

            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright © 2014-2015 <a href="#">CBN Quaters</a>.</strong> All rights reserved.
      </footer>

     

    <!-- jQuery 2.1.4 -->
    <script src="js/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="js/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="js/raphael-min.js"></script>
    <script src="js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="js/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="js/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="js/moment.min.js"></script>
    <script src="js/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="js/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="js/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="js/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="js/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js"></script>
  

</body></html>