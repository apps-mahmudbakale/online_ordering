<!DOCTYPE html>
<html class=" js no-touch csstransforms csstransitions" style="" idmmzcc-ext-docid="194766848" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>Najma Memorial Schools Runjin  Sambo Sokoto </title>
<meta name="description" content="Najma Memorial Schools Runjin  Sambo Sokoto">
<meta name="author" content="Mahmud Bakale">

<!-- Favicons-->
<link rel="shortcut icon" href="Resources/Images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" type="image/x-icon" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="Resources/css/megamenu.css" rel="stylesheet">
<link href="Resources/css/style.css" rel="stylesheet">
<link href="Resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="Resources/css/jquery.css">


<!-- REVOLUTION BANNER CSS SETTINGS -->
<link rel="stylesheet" href="Resources/css/fullwidth.css" media="screen">
<link rel="stylesheet" href="Resources/css/settings.css" media="screen">



<!-- Jquery -->
<script src="Resources/js/ga.js" async="" type="text/javascript"></script>
<script src="Resources/js/jquery_006.js"></script>
<!-- Support media queries for IE8 -->
<script src="Resources/js/respond.js"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="Resources/js/modernizr.js"></script>


<!-- Style switcher-->
<link rel="stylesheet" type="text/css" media="screen,projection" href="Resources/css/jquery-sticklr-1.css">
<!-- Fonts-->
<link rel="alternate stylesheet" type="text/css" href="Resources/css/helvetica.css" title="helvetica" media="all">


</head>

<body>
<header>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4" id="logo"><a href="index.php"><img src="Resources/Images/logo.fw.png" alt="Logo"></a></div>
        <div class="col-md-8 col-sm-8">
        
            

        </div><!-- End col-md-8-->
        </div><!-- End row-->
    </div><!-- End container-->
</header><!-- End Header-->
    <?php include 'home_nav.php'; ?>    
    <!--Start slider-->
<div id="carousel-example-generic" class="carousel slide fullwidthbanner-container" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item">
                                <img src="Resources/Images/slide-1.jpg">
                            </div>
                            <div class="item">
                                <img src="Resources/Images/slide-2.jpg">
                            </div>
                            <div class="item active">
                                <img src="Resources/Images/slide-3.png">
                            </div>
                        </div>
                    </div>
 <!--End slider-->

<div class="container">
<!--
     <div class="row" id="main-boxes">
        <div class="col-md-4 col-sm-4">
            <div class="box-style-2 green">
                <a href="login.php" title="All Courses">
                   <img src="Resources/Images/icon-home-visit.png" alt="">
                    <h3>Login</h3>
                    <p>Login into the Portal, click to login</p>
                </a>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-4" style="margin-left: 344px;">
            <div class="box-style-2 green">
                <a href="Application.php" title="Apply">
                    <img src="Resources/Images/icon-home-apply.png" alt="">
                    <h3>Apply now</h3>
                    <p>The Application for admission into the our Schools is now open, click to apply</p>
                </a>
            </div>
        </div>
        
    </div>-->
    <nav></nav>
</div> 


<div class="container">

<div class="row">
    
    <!-- =========================Start Col left section ============================= -->   
    <aside class="col-md-4 col-sm-4">
    <div class="col-left">
        <h3>Latest news</h3>
        <div class="widget">
            <ul class="latest_news">
                
            </ul>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>
    </div><!--End col left -->
    </aside>

<!-- =========================Start Col right section ============================= --> 
<section class="col-md-8 col-sm-8">
    <div class="col-right">
       <div class="main-img">
            <img src="Resources/Images/f3.jpg" alt="" class="img-responsive">
            <p class="lead">Welcome to Najma Memorial Schools Sokoto State. </p>
        </div>
    
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

   <h5>Aims and Objectives</h5>
    <ul class=" list_3">
        <li><a href="#"><i class="fa fa-arrow-right"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.. </a></li>

        <li><a href="#"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum..</a></li>
    </ul>
   <p>somethinhg new...........</p>

   

    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>


<hr>
        </div><!-- end col right-->
    </section>
    </div><!-- end row-->
</div> <!-- end container-->


<footer>
  <div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-4" id="brand-footer">
            <p><img src="Resources/Images/logo-footer.png" alt=""></p>
            <p>Copyright © <?php echo date('Y');?></p> 
            <p>Site Designed By <a href="">Bnetworks </a></p> 
        </div>
        <div class="col-md-4 col-sm-4" id="contacts-footer">
            <h4>Contacts</h4>
            <ul>
                <li><i class="icon-home"></i>Along Sokoto Runjin Sambo Road, Sokoto - Nigeria</li>
                <li><i class="icon-phone"></i> Telephone: +234 70 6426 3468</li>
                <li><i class="icon-phone-sign"></i> Fax: +234 60 234 642</li>
                <li><i class="icon-envelope"></i> Email: <a href="#">info@najmaschools.com</a></li>
            </ul>
            <hr>
            <h4>Newsletter</h4>
            <p>Subscribe for our news letter.</p>
            
            <div id="message-newsletter"></div>
              <form method="post" action="assets/newsletter.php" name="newsletter" id="newsletter" class="form-inline">
                <input name="email_newsletter" id="email_newsletter" placeholder="Your Email" class="form-control" type="email">
                <button id="submit-newsletter" class="button_medium add-bottom-20" style="top:2px; position:relative"> Subscribe</button>
              </form>
            </div>
        
    </div>
  </div>
  </footer><!-- End footer-->
<div style="display: block;" id="toTop">Back to Top</div>

<!-- MEGAMENU --> 
<script src="Resources/js/jquery_003.js"></script>
<script src="Resources/js/megamenu.js"></script>

<!-- OTHER JS -->    
<script src="Resources/js/bootstrap.js"></script>
<script src="Resources/js/functions.js"></script>
<script src="Resources/js/validate.js"></script> 

<!-- FANCYBOX -->
<script src="Resources/js/jquery_002.js" type="text/javascript"></script> 
<script src="Resources/js/jquery_005.js" type="text/javascript"></script>

 <!-- REVOLUTION SLIDER -->
 <script src="Resources/js/jquery_004.js"></script>
 <script type="text/javascript" src="Resources/js/jquery.js"></script>
</body>
</html>