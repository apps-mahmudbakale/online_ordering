<?php
class Html{
	public static function start($title){
		echo('

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>'.$title.'</title>

    <!-- Bootstrap Core CSS -->
    <link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="Resources/bootstrap/css/full-slider.css" rel="stylesheet">
    <link href="Resources/eternicode/css/bootstrap-datepicker3.css" rel="stylesheet">
    <link href="Resources/material-design/bootstrap-material-design.css" rel="stylesheet">
    <link href="Resources/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="Resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="css/style.css" media="screen" rel="stylesheet">

    <link href="css/main.css" media="screen" rel="stylesheet">

<style>
body{
    background: linear-gradient(#0CAB89, #052612);
}

</style>

</head>

<body>

');
	}

public static function end(){
echo("    <script src='Resources/jquery/dist/jquery.min.js'></script>

    <!-- Bootstrap Core JavaScript -->
    <script src='Resources/bootstrap/js/bootstrap.min.js'></script>

<script src='Resources/eternicode/dist/js/bootstrap-datepicker.js'></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src='Resources/timeago/moment.js'></script>
    <script src='Resources/timeago/livestamp.js'></script>


    <!-- Custom Theme JavaScript -->
    <script type=text/javascript' src='dist/js/sb-admin-2.js'></script>
    <script type='text/javascript' src='js/script.js'></script>
    <script type='text/javascript' src='js/signup.js'></script>
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
  </body>

</html>

	");
}

}
?>