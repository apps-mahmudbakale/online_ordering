<!doctype html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="./.favicon.ico">
   <title>Smurf Directory</title>

   <link rel="stylesheet" href="../.style.css">
   <link rel="stylesheet"  href="../.css/bootstrap.min.css">
   <link rel="stylesheet"  href="../.css/font-awesome.min.css">
   <script src="./.sorttable.js"></script>
</head>

<body>
<div id="container">
	<h1>Welcome To Your Projects Directory Smurfy</h1>
	<?php 

                  	if (isset($_POST['submit'])) {
                  		if (!empty($_POST['project'])) {
                  			   echo $project = $_POST['project'];
                  				@mkdir($project);
                  				copy('./.index.php', $project.'/.index.php');
                  				copy('./.index.php', $project.'/.error.php');
                  			if (!empty($_POST['include'])) {
                  				if (is_dir($project.'/Includes/')) {
                  						copy('./.files/includes/connection.php', $project.'/Includes/connection.php');
                  						copy('./.files/includes/init.php', $project.'/Includes/init.php');
                  					}else{
                  						@mkdir($project.'/Includes/');
                  						copy('./.files/includes/connection.php', $project.'/Includes/connection.php');
                  						copy('./.files/includes/init.php', $project.'/Includes/init.php');
                  					}	
                  				

                  				
                  			}

                  			if (!empty($_POST['layout'])) {
                  				if (is_dir($project.'/Layouts/')) {
                  						copy('./.files/Layouts/Html.php', $project.'/Layouts/Html.php');
                  						copy('./.files/Layouts/Navs.php', $project.'/Layouts/Navs.php');
                  						copy('./.files/Layouts/Forms.php', $project.'/Layouts/Forms.php');
                  						copy('./.files/Layouts/Media.php', $project.'/Layouts/Media.php');
                  					}else{
                  						@mkdir($project.'/Layouts/');
                  						copy('./.files/Layouts/Html.php', $project.'/Layouts/Html.php');
                  						copy('./.files/Layouts/Navs.php', $project.'/Layouts/Navs.php');
                  						copy('./.files/Layouts/Forms.php', $project.'/Layouts/Forms.php');
                  						copy('./.files/Layouts/Media.php', $project.'/Layouts/Media.php');
                  					}	
                  			}

                  			
                  		}


                  	}


                  	 ?>
             <a href="../.index.php" class="btn btn-success"><i class="fa fa-home"></i> Home</a>
			<a href="#new"  data-toggle="modal" class="btn btn-success"><i class="fa fa-folder-open"></i> New Project</a>
			<a href="" class="btn btn-success"><i class="fa fa-file-zip-o"></i> Zip And Download Folder</a>
			<a href="" class="btn btn-success"><i class="fa fa-database"></i> Backup database</a>
	<table class="sortable">
	    <thead>
		<tr>
			<th>Filename</th>
			<th>Type</th>
			<th>Size</th>
			<th>Date Modified</th>
		</tr>
	    </thead>
	    <tbody><?php

	// Adds pretty filesizes
	function pretty_filesize($file) {
		$size=filesize($file);
		if($size<1024){$size=$size." Bytes";}
		elseif(($size<1048576)&&($size>1023)){$size=round($size/1024, 1)." KB";}
		elseif(($size<1073741824)&&($size>1048575)){$size=round($size/1048576, 1)." MB";}
		else{$size=round($size/1073741824, 1)." GB";}
		return $size;
	}

 	// Checks to see if veiwing hidden files is enabled
	if($_SERVER['QUERY_STRING']=="hidden")
	{$hide="";
	 $ahref="./";
	 $atext="Hide";}
	else
	{$hide=".";
	 $ahref="./?hidden";
	 $atext="Show";}

	 // Opens directory
	 $myDirectory=opendir(".");

	// Gets each entry
	while($entryName=readdir($myDirectory)) {
	   $dirArray[]=$entryName;
	}

	// Closes directory
	closedir($myDirectory);

	// Counts elements in array
	$indexCount=count($dirArray);

	// Sorts files
	sort($dirArray);

	// Loops through the array of files
	for($index=0; $index < $indexCount; $index++) {

	// Decides if hidden files should be displayed, based on query above.
	    if(substr("$dirArray[$index]", 0, 1)!=$hide) {

	// Resets Variables
		$favicon="";
		$class="file";

	// Gets File Names
		$name=$dirArray[$index];
		$namehref=$dirArray[$index];

	// Gets Date Modified
		$modtime=date("M j Y g:i A", filemtime($dirArray[$index]));
		$timekey=date("YmdHis", filemtime($dirArray[$index]));


	// Separates directories, and performs operations on those directories
		if(is_dir($dirArray[$index]))
		{
				$extn="&lt;Directory&gt;";
				$size="&lt;Directory&gt;";
				$sizekey="0";
				$class="dir";

			// Gets favicon.ico, and displays it, only if it exists.
				if(file_exists("$namehref/favicon.ico"))
					{
						$favicon=" style='background-image:url($namehref/favicon.ico);'";
						$extn="&lt;Website&gt;";
					}

			// Cleans up . and .. directories
				if($name=="."){$name=". (Current Directory)"; $extn="&lt;System Dir&gt;"; $favicon=" style='background-image:url($namehref/.favicon.ico);'";}
				if($name==".."){$name=".. (Parent Directory)"; $extn="&lt;System Dir&gt;";}
		}

	// File-only operations
		else{
			// Gets file extension
			$extn=pathinfo($dirArray[$index], PATHINFO_EXTENSION);

			// Prettifies file type
			switch ($extn){
				case "png": $extn="PNG Image"; break;
				case "jpg": $extn="JPEG Image"; break;
				case "jpeg": $extn="JPEG Image"; break;
				case "svg": $extn="SVG Image"; break;
				case "gif": $extn="GIF Image"; break;
				case "ico": $extn="Windows Icon"; break;

				case "txt": $extn="Text File"; break;
				case "log": $extn="Log File"; break;
				case "htm": $extn="HTML File"; break;
				case "html": $extn="HTML File"; break;
				case "xhtml": $extn="HTML File"; break;
				case "shtml": $extn="HTML File"; break;
				case "php": $extn="PHP Script"; break;
				case "js": $extn="Javascript File"; break;
				case "css": $extn="Stylesheet"; break;

				case "pdf": $extn="PDF Document"; break;
				case "xls": $extn="Spreadsheet"; break;
				case "xlsx": $extn="Spreadsheet"; break;
				case "doc": $extn="Microsoft Word Document"; break;
				case "docx": $extn="Microsoft Word Document"; break;

				case "zip": $extn="ZIP Archive"; break;
				case "htaccess": $extn="Apache Config File"; break;
				case "exe": $extn="Windows Executable"; break;

				default: if($extn!=""){$extn=strtoupper($extn)." File";} else{$extn="Unknown";} break;
			}

			// Gets and cleans up file size
				$size=pretty_filesize($dirArray[$index]);
				$sizekey=filesize($dirArray[$index]);
		}

	// Output
	 echo("
		<tr class='$class'>
			<td><a href='./$namehref'$favicon class='name'>$name</a></td>
			<td><a href='./$namehref'>$extn</a></td>
			<td sorttable_customkey='$sizekey'><a href='./$namehref'>$size</a></td>
			<td sorttable_customkey='$timekey'><a href='./$namehref'>$modtime</a></td>
		</tr>");
	   }
	}
	?>

	    </tbody>
	</table>

	<h2><?php echo("<a href='$ahref'>$atext hidden files</a>"); ?></h2>
</div>

<script src="./.js/jquery.js"></script>
<script src="./.js/bootstrap.min.js"></script>
<!--dia-->
 <div class="modal modal-primary fade" id="new" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                      <h4 class="modal-title"><i class="fa fa-folder-open"></i> New Project</h4>
                  </div>
                  <div class="modal-body">
                  <div class="panel panel-success">
                  	<div class="panel-heading">Enter Project Details</div>
                  	<div class="panel-body">
                     <form action=""  method="post" role="form">
                         <div class="form-group">
                             <input type="text" id="username" name="project" placeholder="Project Tiltle" required class="form-control">
                             <div id="title_text" style="color:#a9302a"></div>
                         </div>
                         <fieldset>

                         	<legend>Packages</legend>
                         	<div class="col-md-3"><h4>Includes</h4><input type="checkbox" name="include" value="include"></div>
                         	
                         	<div class="col-md-3"><h4>Layouts</h4> <input type="checkbox" name="layout" value="layout"></div>
                         </fieldset>
                  </div>
                  <div class="modal-footer">
                      <button type="submit"  id="submit" name="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Submit</button>
                  </div>
                  </form>
                  </div>
                  </div>
				  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->

</body>
</html>
