<?php 
include 'includes/init.php';
 ?>
<!DOCTYPE html>
<html class=" js no-touch csstransforms csstransitions" style="" idmmzcc-ext-docid="194766848" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>Najma Memorial Schools Runjin  Sambo Sokoto </title>
<meta name="description" content="Najma Memorial Schools Runjin  Sambo Sokoto">
<meta name="author" content="Mahmud Bakale">

<!-- Favicons-->
<link rel="shortcut icon" href="Resources/Images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" type="image/x-icon" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="Resources/css/megamenu.css" rel="stylesheet">
<link href="Resources/css/app.css" rel="stylesheet">
<link href="Resources/css/style.css" rel="stylesheet">
<link href="Resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="Resources/css/AdminLTE.min.css">
<link rel="stylesheet" type="text/css" href="Resources/skins/_all-skins.css">
<link rel="stylesheet" href="Resources/css/jquery.css">


<!-- REVOLUTION BANNER CSS SETTINGS -->
<link rel="stylesheet" href="Resources/css/fullwidth.css" media="screen">
<link rel="stylesheet" href="Resources/css/settings.css" media="screen">



<!-- Jquery -->
<script src="Resources/js/ga.js" async="" type="text/javascript"></script>
<script src="Resources/js/jquery_006.js"></script>
<!-- Support media queries for IE8 -->
<script src="Resources/js/respond.js"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="Resources/js/modernizr.js"></script>


<!-- Style switcher-->
<link rel="stylesheet" type="text/css" media="screen,projection" href="Resources/css/jquery-sticklr-1.css">
<!-- Fonts-->
<link rel="alternate stylesheet" type="text/css" href="Resources/css/helvetica.css" title="helvetica" media="all">


</head>

<body>
<header>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4" id="logo"><a href="index.php"><img src="Resources/Images/logo.png" alt="Logo"></a></div>
        <div class="col-md-8 col-sm-8">
        
            <div id="phone" class="hidden-xs"><strong>+234 803 650 9472 </strong>Admissions department</div>
            <div id="phone" class="hidden-xs"><strong>info@najmaschools.com </strong>Admissions department</div>

        </div><!-- End col-md-8-->
        </div><!-- End row-->
    </div><!-- End container-->
</header><!-- End Header-->
    <?php include 'admin_nav.php'; ?>    
<div class="" style="margin-top:-30px;">

<div class="hbox stretch" style="height: 439px">
    
    <!-- =========================Start of Application Form ============================= -->   
        <?php 
        include 'head_teach_sidebar.php';

         ?>

          <div class="col-lg-10">
            <legend><i class="fa fa-home"></i> Head Teachers Home</legend>
            As administrator in the portal you can manage all the activities of the site
          </div>
  <div class="col-lg-2 col-md-2 col-sm-10 col-xs-12">
  <div class="panel panel-success">
     <div class="panel-heading">Hello! <?php echo " Sir" ?></div>
     <div class="panel-body" style="text-align:center">
        <i  class="fa fa-user-md fa-2x"></i>
     </div>
  </div>
</div>
    <!-- =========================End of Application Form ============================= -->  

<hr>
        </div><!-- end col right-->
    </section>
    </div><!-- end row-->
</div> <!-- end container-->


<footer>
  <div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-4" id="brand-footer">
            <p><img src="Resources/Images/logo-footer.png" alt=""></p>
            <p>Copyright © <?php echo date('Y');?></p> 
            <p>Site Designed By <a href="">Bnetworks </a></p> 
        </div>
        <div class="col-md-4 col-sm-4" id="contacts-footer">
            <h4>Contacts</h4>
            <ul>
                <li><i class="icon-home"></i>Along Sokoto Runjin Sambo Road, Sokoto - Nigeria</li>
                <li><i class="icon-phone"></i> Telephone: +234 70 6426 3468</li>
                <li><i class="icon-phone-sign"></i> Fax: +234 60 234 642</li>
                <li><i class="icon-envelope"></i> Email: <a href="#">info@najmaschools.com</a></li>
            </ul>
            <hr>
            <h4>Newsletter</h4>
            <p>Subscribe for our news letter.</p>
            
            <div id="message-newsletter"></div>
              <form method="post" action="assets/newsletter.php" name="newsletter" id="newsletter" class="form-inline">
                <input name="email_newsletter" id="email_newsletter" placeholder="Your Email" class="form-control" type="email">
                <button id="submit-newsletter" class="button_medium add-bottom-20" style="top:2px; position:relative"> Subscribe</button>
              </form>
            </div>
        
    </div>
  </div>
  </footer><!-- End footer-->
<div style="display: block;" id="toTop">Back to Top</div>

<!-- MEGAMENU --> 
<script src="Resources/jquery/dist/jquery.js"></script>
<script src="Resources/js/jquery_003.js"></script>
<script src="Resources/js/megamenu.js"></script>

<!-- OTHER JS -->    
<script src="Resources/js/bootstrap.js"></script>
<script src="Resources/js/functions.js"></script>
<script src="Resources/js/validate.js"></script> 

<!-- FANCYBOX -->
<script src="Resources/js/jquery_002.js" type="text/javascript"></script> 
<script src="Resources/js/jquery_005.js" type="text/javascript"></script>

 <!-- REVOLUTION SLIDER -->
 <script src="Resources/js/jquery_004.js"></script>
 <script type="text/javascript" src="Resources/js/jquery.js"></script>
 <script type="text/javascript">
  $(document).ready(function(e){
  $('#state_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxLga.php',
            data: 'state_id='+$(this).val(),
            success:function(r) {
                $('#lga_id').html(r);
                //alert(r);
            }
        })
   })

  $('#sta').load('check.php').show();

  $('#card_n').keyup(function(){
      $('#status').append('a');
      $.post('check.php', { card: form.card_number.value}, function(result) {
        $('#sta').html(result).show();
      });
  });
  
});
</script>
</body>
</html>