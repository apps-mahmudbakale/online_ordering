<?php 
include 'includes/init.php';
 ?>
<!DOCTYPE html>
<html class=" js no-touch csstransforms csstransitions" style="" idmmzcc-ext-docid="194766848" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>Najma Memorial Schools Runjin  Sambo Sokoto </title>
<meta name="description" content="Najma Memorial Schools Runjin  Sambo Sokoto">
<meta name="author" content="Mahmud Bakale">

<!-- Favicons-->
<link rel="shortcut icon" href="Resources/Images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" type="image/x-icon" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="Resources/css/megamenu.css" rel="stylesheet">
<link href="Resources/css/style.css" rel="stylesheet">
<link href="Resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="Resources/css/jquery.css">


<!-- REVOLUTION BANNER CSS SETTINGS -->
<link rel="stylesheet" href="Resources/css/fullwidth.css" media="screen">
<link rel="stylesheet" href="Resources/css/settings.css" media="screen">



<!-- Jquery -->
<script src="Resources/js/ga.js" async="" type="text/javascript"></script>
<script src="Resources/js/jquery_006.js"></script>
<!-- Support media queries for IE8 -->
<script src="Resources/js/respond.js"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="Resources/js/modernizr.js"></script>


<!-- Style switcher-->
<link rel="stylesheet" type="text/css" media="screen,projection" href="Resources/css/jquery-sticklr-1.css">
<!-- Fonts-->
<link rel="alternate stylesheet" type="text/css" href="Resources/css/helvetica.css" title="helvetica" media="all">


</head>

<body>
<header>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4" id="logo"><a href="index.php"><img src="Resources/Images/logo.png" alt="Logo"></a></div>
        <div class="col-md-8 col-sm-8">
        
            <div id="phone" class="hidden-xs"><strong>+234 803 650 9472 </strong>Admissions department</div>
            <div id="phone" class="hidden-xs"><strong>info@najmaschools.com </strong>Admissions department</div>

        </div><!-- End col-md-8-->
        </div><!-- End row-->
    </div><!-- End container-->
</header><!-- End Header-->
    <?php include 'home_nav.php'; ?>    
<div class="container">

<div class="row">
    
    <!-- =========================Start of Application Form ============================= -->   

    <fieldset>
<?php
  if(isset($_POST["submit"])) {
    $username = clean($_POST['username']);
    $password = clean($_POST['password']);
    $password2 = clean($_POST['cpassword']);
    $fname = clean($_POST['firstname']);
    $onames = clean($_POST['lastname']);
    $phone = clean($_POST['phone']);
    $address = clean($_POST['address']);
    $class_id = clean($_POST['class_id']);
    $lga_id = clean($_POST['lga_id']);
    $p = clean($_POST['fn']);
    $pp = clean($_POST['gphone']);
    $c = clean($_POST['card_number']);
    $gender = clean($_POST['gender']);
    $dob = clean($_POST['dob']);

    if($password == $password2) {
       $rows = Sql::SqlRows("applicant_login",array('username'),array($username));
       if($rows == 0) {
           $rows2 = Sql::SqlRows("pin",array('card_number'),array($c)); 
           if($rows2 == 1) {
            Sql::Insert("applicant_login",array('username','password'),array($username,md5($password)));
            $query = "SELECT MAX(app_id) AS app_id FROM applicant_login";
            $res = mysqli_query($db, $query) or die(mysql_error());
            $row = mysqli_fetch_array($res);
            $app_id = $row["app_id"];
            $image = "passports/".sprintf("%04s",$app_id).".jpg";
            move_uploaded_file($_FILES['file']['tmp_name'],$image);
            Sql::Insert("applicant",array('app_id','card_number',
              'first_name','other_names','image','gender','date_of_birth','lga_id','guardian_full_name','guardian_phone','phone_number','address','class_id'),
                      array($app_id,$c,$fname,$onames,$image,$gender,$dob,$lga_id,$p,$pp,$phone,$address,$class_id));
            mysqli_query($db,"DELETE FROM pin WHERE card_number='$c'");
            echo "<script>window.location='print.php?app_id=$app_id'</script>";
        } else {
            echo "<script>alert('Invalid Card Number')</script>";
        }
       } else echo "<script>alert('Change your username')</script>";
    } else echo "<script>alert('Password mismatch')</script>";
  }

            
                
            
  ?>       


        <legend><i class="fa fa-pencil"></i> Application Form</legend>
                <form action="" method="POST" name="form" role="form" enctype="Multipart/form-data">
                <div class="col-lg-5">
                    Card Number:
                    <input type="text" id="card_n" name="card_number" pattern="[a-z A-Z0-9]+" required class="form-control"/>
                </div>
                <div class="col-lg-1" id="sta">
                <br>
                </div>
                    <?php 
                        $input = array(array('label' =>'Username', 'name' =>'username', 'type'=>'text', 'id' => '', 'value' =>'', 'holder' =>'Username', 'isDisabled' => false),array('label' =>'Password', 'name' =>'password', 'type'=>'password', 'id' => '', 'value' =>'', 'holder' =>'Password', 'isDisabled' => false),array('label' =>'Confirm Password', 'name' =>'cpassword', 'type'=>'password', 'id' => '', 'value' =>'', 'holder' =>'Confirm Password', 'isDisabled' => false), array('label' =>'First Name', 'name' =>'firstname', 'type'=>'text', 'id' => '', 'value' =>'', 'holder' =>'First Name', 'isDisabled' => false), array('label' =>'Last Name', 'name' =>'lastname', 'type'=>'text', 'id' => '', 'value' =>'', 'holder' =>'Last Name', 'isDisabled' => false));
                        $select = array(array('label' => 'Genader','name' => 'gender', 'id' =>'', 'option' =>array('','Male', 'Female')),array('label' => 'Nationality','name' => 'nationality', 'id' =>'', 'option' =>array('','Nigerian', 'Foreign')));
                         $inputs = array(array('label' =>'Phone Number', 'name' =>'phone', 'type'=>'text', 'id' => '', 'value' =>'', 'holder' =>'Phone Number', 'isDisabled' => false),array('label' =>'Date of Birth', 'name' =>'dob', 'type'=>'date', 'id' => '', 'value' =>@$_POST['dob'], 'holder' =>'Date of Birth', 'isDisabled' => false),array('label' =>'Passport', 'name' =>'file', 'type'=>'file', 'id' => '', 'value' =>'', 'holder' =>'Photo', 'isDisabled' => false));
                        Forms::inputs($input);
                        Forms::select($select);
                        Forms::inputs($inputs);
                        //Forms::F_select('Class', 'class_id', '',  Sql::FillSelect("SELECT * FROM class","class_id","class_name"));

                          

                       ?>  
                <div class="col-lg-6">
                Class:
                <select name="class_id" class="form-control">
                    <?php $select = mysqli_query($db, "SELECT *  FROM class");
                      while($opt = mysqli_fetch_array($select)){
                      echo"<option value='".$row['class_id']."'>".$opt['class_name']."</option>";
                    }
                     ?>
               </select>
               </div>
               <div class="col-lg-6">
                State of Origin:
                 <select name="sate_id" class="form-control" id="state_id">
                  <?php $select2 = mysqli_query($db, "SELECT *  FROM state");
                      while($opt2 = mysqli_fetch_array($select2)){
                      echo"<option value='".$opt2['state_id']."'>".$opt2['state_name']."</option>";
                    }
                     ?>
               </select>
              </div>
              <div class="col-lg-6">
                Local Government:
                <select name="lga_id" class="form-control" id="lga_id">
                   
               </select>
              </div>

                <div class="col-lg-6">
                Guardian  Full Name
                <input type="text" name="fn" pattern="[a-zA-Z ]+" required class="form-control"/>
              </div>

                <div class="col-lg-6">
                Guardian  Phone
                <input type="text" name="gphone" pattern="0[7-9][0-1][0-9]{8}" required class="form-control"/>
              </div>
               <div class="col-lg-12">
                Address
                <textarea class="form-control" name="address"></textarea>
              </div>
              <div class="col-lg-4"> 
              <br>
              <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-pencil"></i> Apply</button>
              </div>
                </form>

    </fieldset>
          
    <!-- =========================End of Application Form ============================= -->  

<hr>
        </div><!-- end col right-->
    </section>
    </div><!-- end row-->
</div> <!-- end container-->


<footer>
  <div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-4" id="brand-footer">
            <p><img src="Resources/Images/logo-footer.png" alt=""></p>
            <p>Copyright © <?php echo date('Y');?></p> 
            <p>Site Designed By <a href="">Bnetworks </a></p> 
        </div>
        <div class="col-md-4 col-sm-4" id="contacts-footer">
            <h4>Contacts</h4>
            <ul>
                <li><i class="icon-home"></i>Along Sokoto Runjin Sambo Road, Sokoto - Nigeria</li>
                <li><i class="icon-phone"></i> Telephone: +234 70 6426 3468</li>
                <li><i class="icon-phone-sign"></i> Fax: +234 60 234 642</li>
                <li><i class="icon-envelope"></i> Email: <a href="#">info@najmaschools.com</a></li>
            </ul>
            <hr>
            <h4>Newsletter</h4>
            <p>Subscribe for our news letter.</p>
            
            <div id="message-newsletter"></div>
              <form method="post" action="assets/newsletter.php" name="newsletter" id="newsletter" class="form-inline">
                <input name="email_newsletter" id="email_newsletter" placeholder="Your Email" class="form-control" type="email">
                <button id="submit-newsletter" class="button_medium add-bottom-20" style="top:2px; position:relative"> Subscribe</button>
              </form>
            </div>
        
    </div>
  </div>
  </footer><!-- End footer-->
<div style="display: block;" id="toTop">Back to Top</div>

<!-- MEGAMENU --> 
<script src="Resources/jquery/dist/jquery.js"></script>
<script src="Resources/js/jquery_003.js"></script>
<script src="Resources/js/megamenu.js"></script>

<!-- OTHER JS -->    
<script src="Resources/js/bootstrap.js"></script>
<script src="Resources/js/functions.js"></script>
<script src="Resources/js/validate.js"></script> 

<!-- FANCYBOX -->
<script src="Resources/js/jquery_002.js" type="text/javascript"></script> 
<script src="Resources/js/jquery_005.js" type="text/javascript"></script>

 <!-- REVOLUTION SLIDER -->
 <script src="Resources/js/jquery_004.js"></script>
 <script type="text/javascript" src="Resources/js/jquery.js"></script>
 <script type="text/javascript">
  $(document).ready(function(e){
  $('#state_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxLga.php',
            data: 'state_id='+$(this).val(),
            success:function(r) {
                $('#lga_id').html(r);
                //alert(r);
            }
        })
   })

  $('#sta').load('check.php').show();

  $('#card_n').keyup(function(){
      $.post('check.php', { card: form.card_number.value}, function(result) {
        $('#sta').html(result).show();
      });
  });
  
});
</script>
</body>
</html>