<?php
/* GridView
   This class generates automatic report
   Created By Umar Muhammad Muhammad Bello
   @FawahirTech
   @2014-2015
   Version: 2.3
*/
@include_once "function.php";
class GridView {
    public $tableClass;
	public static $edition;
	public static $deletion;
	public static $curPage;
	public static $row;
	public $query;
	
    public function getGroupByClause($query) {
       $init = " GROUP BY ";
       $index = strpos($query, $init);
       $endGroupBy = $index + strlen($init);
      
       for($i=$endGroupBy; $i<=strlen($query)-1; $i++) {
       	    if(ord($query[$i]) >= 65)
       	    	$init .= $query[$i];
       }  
       return $init; 
    }

	public static function configQuery($query,$param) {
		$gbs = GridView::getGroupByClause($query);
	    $finalQuery = $query;
		foreach($param as $key=>$value) {
			if(!empty($value)) {
				if($key == 'searchCol') {
					if(!strstr($query, " GROUP BY ")) {
						if(strstr($query, "WHERE"))
					      $finalQuery .= " AND $value LIKE '%$param[searchVal]%' ";
					    else 
					      $finalQuery .= " WHERE $value LIKE '%$param[searchVal]%' ";
					} else {
						$finalQuery =  str_replace(trim($gbs),"",$finalQuery);
					    if(strstr($query, "WHERE"))
					      $finalQuery .= " AND $value LIKE '%$param[searchVal]%' ".$gbs;
					    else 
					      $finalQuery .= " WHERE $value LIKE '%$param[searchVal]%' ".$gbs;
					}
				}
				elseif($key == 'sortCol') {
					   $finalQuery .= " ORDER BY  $value $param[sortType] ";
				}
			}
		}
		
		return $finalQuery;
	}
	public static function searchGrid($searchers) {
		
		echo "<div class='col-lg-12 input-group'>";
		 echo "<div class='col-lg-1 form-control-static'><b>Find</b></div>";
		 echo "<div class='col-lg-6'><input id='text' class='form-control'/></div>";
		 echo "<div class='col-lg-1 form-control-static'><b>By</b></div>";
		 echo "<div class='col-lg-4'><select id='by' class='form-control'>";
		   foreach($searchers as $key => $value) {
		   	  echo "<option value='$key'>$value</option>";
		   }
		 echo "</select></div>";
		echo "</div><p></p>";
	}
	
	private function getCur() {
		return GridView::$curPage;
	}
	public function __construct($query,$columns,$pagination=true,$sorting=false,$edit=true,$delete=true,$perpage=10,$mutiplePaging=false,$queryDetails=true,$more=array()) {
		$this->tableClass = 'col-lg-8 table';
		$s =  @$_GET['s'] == "asc" ? "desc" : "asc";
		//$this->edition='';
		//$this->deletion='';
		$curPage = $this->getCur();
		
		$x = pagination($query,"$_GET[c]&sort=$_GET[s]",$perpage,$curPage);
		$g = $x;
		if($pagination) {
		  
		  $query = $x['query'];
		  echo $this->edition;
		  
		   //echo ($x['pages'] > 1) ? $x['links'] : "";
		}
		if($pagination && $mutiplePaging) {
			echo ($g['pages'] > 1) ? $g['links'] : '';
		}
		$res = mysql_query($query) or die(mysql_error());
		if($queryDetails) {
			$p = isset($_GET['page']) ? $_GET['page'] : 1;
			echo "<div class='col-lg-offset-8 col-lg-4'>";
			if(!$pagination)
				echo "<b>Showing: </b> 0 - ".mysql_num_rows($res);
			else {
				if(mysql_num_rows($res) >0)
				   echo "<b>Showing: </b> ".($x['begin']+1)." - ".($x['begin']+mysql_num_rows($res)) ." of ".$x['total'];
			}
			echo "</div>";
		}
		
		echo "<table class='$this->tableClass'>";
		echo "<tr bgcolor='#F5F5F5'>";
		  echo "<th>S/N</th>";
		  $m = 0;
		 $s = ($_GET['sortType'] !== 'undefined') ? clean($_GET['sortType']) : '';
         $s = ($s == 'ASC') ? $s = 'DESC' : 'ASC';
		  foreach($columns as $c) {
			  if(!$c['key']) {
				  echo "<th class='$c[class]'>";
				  if($sorting==true) {
					checkField($c['data-col']);
					
				   echo "<a href='";
				   echo "javascript:ajaxRefresh(";				   
				    echo '"'.$curPage.'",'.'"'.$c['data-col'].'",'.'"'.$s.'",'
				    		.'"'.$_GET['searchCol'].'",'.'"'.$_GET['searchVal'].'",'.'"'.$_GET['page'].'"';
				   echo ")";
				   echo "'>$c[heading]</a></th>";   
				  }else{
				  echo "$c[heading]</th>";
				  }
			  }
			   
		  }
		 // echo "<input type='text' name='url' value='#".$c['data-col']."&sort=$s'/>";
		  if($edit or $delete or !empty($more)) {
			echo "<th>OPERATIONS</th>";
		  }
		echo "</tr>";
		
		
		$sn=($pagination) ? $x['begin'] : 0;
		
		while($row = mysql_fetch_array($res)) {
			$sn++;
			echo "<tr>";
			echo "<td>$sn</td>";
			$key = "";
			foreach($columns as $c) {
				if(!$c['key']) {
				  if(!$c['link'])
				     echo "<td>".$row[$c['data-col']]."</td>";
				  else {
				  	 echo "<td><a href='$c[link]?id=".$row[$c['data-col-2']]."'>";
				  	 echo (!$c['link-t']) ? $row[$c['data-col']] : $c['link-t'];
				  	echo "</a></td>";
				  }
				}else $key = $c['data-col'];
			}
			 if($edit or $delete or !empty($more)) {
			   echo "<td><div class='btn-group'>";
			     if($edit) {
					 $x = array_unique($row);
					 $all = implode('|',$x);
					  $ed = self::$edition;
					  
					 echo "<button  
					         id='$ed$sn' 
					         class='btn btn-default'
							 data-toggle='modal'
							 value='$all'
							 title='Edit this record'
                             data-target='#edit'><i class='fa fa-edit'></i></button>";
					$all = "";
				 }
				 $del = self::$deletion;
				 
				 if($delete) {
					 echo "<button id='$del$sn'  title='Delete this Record'
					 value='$row[$key]'class='btn btn-default'><i class='glyphicon glyphicon-remove'></i></button>";
				 }
				 if(!empty($more)) {
					 echo "<button 
					         id='$more[id]$sn'
							 class='btn btn-default'
							 data-toggle='modal'
							 value='$row[$key]'
							 title='$more[title]'
							 data-target='#more'><i class='fa $more[icon]'></i></button>";
					          
				 }
			   echo "</div></td>";
		     }
			echo "</tr>";
		}
		echo "</table>";
		if($pagination) {
		 echo ($g['pages'] > 1) ? $g['links'] : '';
		}
	}
}
?>