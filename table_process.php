<?php
session_start(); 
include 'inc/connection.php';
function createRandomPassword() {
    $chars = "003232303232023232023456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '' ;
    while ($i <= 4) {

        $num = rand() % 33;

        $tmp = substr($chars, $num, 1);

        $pass = $pass . $tmp;

        $i++;

    }
    return $pass;
}

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UDUS | Booking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/fontawesome-all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand"><b><i class="fa fa-utensils"></i> UDUS</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="login.php"><i class="fa fa-sign-in-alt"></i> Login<span class="sr-only">(current)</span></a></li>
            <li><a href="table.php"><i class="fa fa-table"></i> Table Booking</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control"  id="navbar-search-input" style="width: 442%; border-radius: 3px;" placeholder="Search">
            </div>
          </form>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Table Booking
        </h1>
        <ol class="breadcrumb">
          <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Table Booiking</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="col-lg-6 thumbnail" style="height: 469px;">
          <legend><i class="fa fa-table"></i> Table Booked</legend>
           <?php
           if (isset($_POST['book'])) {
              $rest = $_SESSION['rest'] = $_POST['rest_id'];
              $table = $_SESSION['table'] = $_POST['table'];
              $time = $_SESSION['time'] = $_POST['time'];
              $date = $_SESSION['date'] = $_POST['date'];
              $query = mysqli_query($db,"SELECT * FROM resturants WHERE rest_id ='$rest'");
              $getr = mysqli_fetch_array($query); 
              
            ?>
          <table class="table table-striped">
            <tr>
              <td>Restaurant</td>
              <td><strong><?php echo $getr['rest_name'] ?></strong></td>
            </tr>
            <tr>
              <td>People in Number</td>
              <td><strong><?php echo $table ?></strong></td>
            </tr>
             <tr>
              <td>Time</td>
              <td><strong><?php echo $time ?></strong></td>
            </tr>
             <tr>
              <td>Date</td>
              <td><strong><?php echo $date ?></strong></td>
            </tr>
          </table>
          <em>
            <b>Note:</b> Payment of All Booking Should Be at Respective Booked Restaurants Unless if You wish to <form >
  <script src="https://js.paystack.co/v1/inline.js"></script>
  <button type="button" class="btn btn-success"  id="pay" onclick="payWithPaystack()"><i class="fa fa-money"></i> Pay Now</button> 
</form>
 
<script>
  function payWithPaystack(){
    var handler = PaystackPop.setup({
      key: 'pk_test_30bbe756476a4c8b47a3df7a2201cd711b16ceb1',
      email: 'apps.mahmud.bakale@gmail.com',
      amount: 10000,
      currency: "NGN",
      ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
      metadata: {
         custom_fields: [
            {
                display_name: "Mobile Number",
                variable_name: "mobile_number",
                value: "+23425152258"
            }
         ]
      },
      callback: function(response){
          alert('success. transaction ref is ' + response.reference);
      },
      onClose: function(){
          alert('window closed');
      }
    });
    handler.openIframe();
  }
</script>
          </em>
          <?php }
          if (isset($_POST['process'])) {
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $phone =  $_POST['phone'];
                $email = $_POST['email'];
                $address = $_POST['address'];
                $rest = $_SESSION['rest'];
                $table = $_SESSION['table'];
                $time = $_SESSION['time'];
                $date = $_SESSION['date'];
                $ref = "UDUS-".createRandomPassword();
                mysqli_query($db,"INSERT INTO `customers`(`customer_id`, `firstname`, `lastname`, `phone`, `email`, `address`) VALUES (NULL,'$firstname','$lastname','$phone','$email','$address')");
                  $cust = mysqli_query($db,"SELECT MAX(customer_id) As customer FROM customers");
                  $getc = mysqli_fetch_array($cust);
                  $customer = $getc['customer'];

                  $insert = mysqli_query($db,"INSERT INTO `table_orders`(`id`, `customer_id`, `table`, `rest_id`, `time`, `date`,`ref`) VALUES (NULL,'$customer','$table','$rest','$time','$date','$ref')");

                  if ($insert) {
                    echo "<script>window.location='table_success.php?id=".$rest."&&cust=".$customer."'</script>";
                  }

              }

           ?>

        </div>
        <div class="col-lg-6 thumbnail" style="height: 469px;">
          <fieldset>
            <legend> <i class="fa fa-user"></i> Customer Details</legend>
            <form action="" method="POST">
              <div class="col-lg-6">
                First Name
                <input type="text" name="firstname" class="form-control">
              </div>
              <div class="col-lg-6">
                Last Name
                <input type="text" name="lastname" class="form-control">
              </div>
              <div class="col-lg-6">
                Phone
                <input type="text" name="phone" class="form-control">
              </div>
               <div class="col-lg-6">
                Email
                <input type="email" name="email" class="form-control">
              </div>
               <div class="col-lg-12">
                Address
                <textarea name="address" id="" cols="30" rows="10" class="form-control"></textarea>
              </div>
              <div class="col-lg-6">
               <br>
                <input type="submit" name="process" value="Process Booking" class="btn btn-success">
              </div>
            </form>
          </fieldset>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Designed By Asmau Tsamiya</b> 
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="">UDUS RESTAURANTS</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->

<script>
   $(document).ready(()=>{
    $('#pay').click(()=>{
      alert('yes');
    })
   });
</script>

<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
