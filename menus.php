<?php 
session_start();
include 'inc/connection.php';
$id = $_GET['id'];

$query = mysqli_query($db, "SELECT * FROM products WHERE rest_id = '$id'");
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UDUS | Menus</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="css/fontawesome-all.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand"><b><i class="fa fa-utensils"></i> UDUS</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="login.php"><i class="fa fa-sign-in-alt"></i> Login<span class="sr-only">(current)</span></a></li>
            <li><a href="table.php"><i class="fa fa-table"></i> Table Booking</a></li>
          
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control"  id="navbar-search-input" style="width: 355%; border-radius: 3px;" placeholder="Search">
            </div>
          </form>
        </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li>
              <a href="cart.php">
                <i class="fa fa-shopping-cart"></i>
                <span class="hidden-xs">Cart <label class="label label-warning"><?php echo count(@$_SESSION['cart']) ?></label></span>
              </a>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Menu Result
          <?php 
            $count = mysqli_num_rows($query);
           ?>
          <small>About (<?php echo $count ?>) Results Found</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Search</a></li>
          <li class="active">meal Menu</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <?php 
          while ($row = mysqli_fetch_array($query)) {
            //print_r($row);
              if (!empty($row)) {?>

      <div class="col-lg-3">
          <div class="panel panel-danger" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-gift'></i> <?php echo strtoupper($row['product_name']); ?></div>
          <div class="panel-body">
              <img class="img img-responsive" width="200px" height="10px" src="captions/<?php echo $row['product_caption'];?>">
              <hr>
              <tr>
                <td><?php echo $row['product_name'] ?></td>
                <td>&#8358; <?php echo number_format($row['product_price'],2)?></td>
              </tr>
        </div>
        <div class='panel-footer'>
          <form action="parse_cart.php" method="POST">
              <input type="hidden" name="page" value="<?php echo $_GET['id']; ?>">
              <input type="hidden" name="meal_id" value="<?php echo $row['product_id'] ?>">
              <input type="hidden" name="qty" value="1">
              <button type="submit" name="buy" class="btn btn-warning"> <i class="fa fa-shopping-cart"></i> Add to cart</button>
          </form>
         
        </div>
         
      </div>
  </div>
                
             <?php }
          }
         ?>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Designed By Asmau Tsamiya</b> 
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="">UDUS RESTAURANTS</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
