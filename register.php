<?php
	include 'inc/connection.php';
  // if (isset($_POST['submit'])) {
    
		// 	$firstname = $_POST['fname'];
		// 	$lastname  = $_POST['lname'];
		// 	$phone = $_POST['phone'];
		// 	$email = $_POST['email'];
		// 	$password = md5($_POST['password']);
	
		// 	$query = mysqli_query($db,"INSERT INTO `accounts`(`account_id`, `firstname`, `lastname`, `phone`, `email`,`password`) VALUES (NULL,'$firstname','$lastname','$phone','$email','$password')");
		// 	if ($query) {
		// 		echo "<script>window.location='Success.php'</script>";
		// 	}

  //   }

 ?>
 <!DOCTYPE html>
 <html>
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Snacks | Registration Page</title>
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3.3.7 -->
   <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
   <!-- Font Awesome -->
   <link rel="stylesheet" href="css/fontawesome-all.css">
   <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
   <!-- Ionicons -->
   <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
   <!-- Theme style -->
   <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
   <!-- iCheck -->
   <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
   <style media="screen">
   body{
     background-image: url(images/1.jpg) !important;
     background-size: cover !important;
     background-repeat: no-repeat !important;
   }
   .register-box-body{
     background-color: rgba(255, 255, 255, 0.8);
   }
   </style>

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->

   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 </head>
 <body class="hold-transition register-page">
 <div class="register-box">
   <div class="register-logo">
     <a href="index.php"><b style="color:white"><i class="fas fa-utensils"></i> Snacks</b></a>
   </div>
    <div id="feedback"></div>
   <div class="register-box-body">
     <p class="login-box-msg">Register a new membership</p>

     <form action="" method="post">
       <div class="form-group has-feedback">
         <input type="text" name="fname" id="fname" class="form-control" placeholder="First Name">
         <span class="glyphicon glyphicon-user form-control-feedback"></span>
       </div>
       <div class="form-group has-feedback">
         <input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name">
         <span class="glyphicon glyphicon-user form-control-feedback"></span>
       </div>
       <div class="form-group has-feedback">
         <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone..">
         <span class="glyphicon glyphicon-phone form-control-feedback"></span>
       </div>
       <div class="form-group has-feedback">
         <input type="email" name="email" id="email" class="form-control" placeholder="Email">
         <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
       </div>
       <div class="form-group has-feedback">
         <input type="password" name="password" id="password" class="form-control" placeholder="Password">
         <span class="glyphicon glyphicon-lock form-control-feedback"></span>
       </div>
       <div class="form-group has-feedback">
         <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Retype password">
         <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
       </div>
       <div class="row">
         <div class="col-xs-8">
           <div class="checkbox icheck">
             <label>
               <input type="checkbox"> I agree to the <a href="#">terms</a>
             </label>
           </div>
         </div>
         <!-- /.col -->
         <div class="col-xs-4">
           <button type="submit" name="submit" id="signup" class="btn btn-primary btn-block btn-flat">Register</button>
         </div>
         <!-- /.col -->
       </div>
     </form>
   </div>
   <!-- /.form-box -->
 </div>
 <!-- /.register-box -->

 <!-- jQuery 3 -->
 <script src="bower_components/jquery/dist/jquery.min.js"></script>
 <!-- Bootstrap 3.3.7 -->
 <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 <!-- iCheck -->
 <script src="plugins/iCheck/icheck.min.js"></script>
 <script>
   $(function () {
     $('input').iCheck({
       checkboxClass: 'icheckbox_square-blue',
       radioClass: 'iradio_square-blue',
       increaseArea: '20%' /* optional */
     });
   });
 </script>
 <script>
  $(document).ready(function(){
    $('#signup').click(function() {
      var fname = $('#fname').val();
      var lname = $('#lname').val();
      var email = $('#email').val();
      var phone = $('#phone').val();
      var password = $('#password').val();
      var cpassword = $('#cpassword').val();

      if (password == cpassword) {
        $.ajax({
        url: 'reg_parse.php',
        type: 'post',
        data: {email: email, fname: fname, lname: lname, phone: phone, password:password},
        success: function(data) {
          alert('Success');
          window.location='login.php';
        }
      })
      }
      
    })
  })
</script>
 </body>
 </html>
